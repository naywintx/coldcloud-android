package network.revolutions.app.coldcloud.object;

import network.revolutions.app.coldcloud.R;

public class CFStatus {

    public String name;
    public String state;
    public int color = GREEN;

    public static final int GREEN = R.color.cf_status_green;
    public static final int ORANGE = R.color.cf_status_orange;
    public static final int RED = R.color.cf_status_red;

}

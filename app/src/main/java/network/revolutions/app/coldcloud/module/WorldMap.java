package network.revolutions.app.coldcloud.module;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Collections;

import network.revolutions.app.coldcloud.R;
import network.revolutions.app.coldcloud.fragment.FragmentDashboard;
import network.revolutions.app.coldcloud.object.CountryStat;
import network.revolutions.app.coldcloud.ui.ViewManager;

public class WorldMap extends Module {

    private final static int MAX = 8;

    public static JSONObject countryList = null;
    private CountryStat stat;
    private LayoutInflater inflater;
    private ProgressBar progress;
    private LinearLayout table;

    public WorldMap(FragmentDashboard parent, ScrollView scrollView) {
        super(parent, scrollView);
    }

    @Override
    public void onDraw(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.module_world_map, container, false);

        this.inflater = inflater;
        progress = view.findViewById(R.id.map_progress);
        table = view.findViewById(R.id.world_table);
        view.findViewById(R.id.world_show_more).setOnClickListener(v -> showWorldView());

        loadCountryList(parent.requireContext());

        container.addView(view);
    }

    private void showWorldView() {
        if (stat == null) {
            Toast.makeText(parent.requireContext(), R.string.wait_for_data, Toast.LENGTH_SHORT).show();
            return;
        }

        parent.setView(ViewManager.VIEW_WORLD, stat);
    }


    public void update(CountryStat stat) {
        // sort
        Collections.sort(stat.countries, (c1, c2) -> c2.requests - c1.requests);
        this.stat = stat;

        drawTable(stat, table, inflater, false);
        progress.setVisibility(View.INVISIBLE);
    }

    @SuppressLint("DefaultLocale")
    public static void drawTable(CountryStat stat, ViewGroup container, LayoutInflater inflater, boolean showAll) {
        boolean lastRow = stat.countries.size() > MAX && !showAll;
        int max = showAll ? stat.countries.size() : Math.min(stat.countries.size(), MAX);

        // create table
        container.removeAllViews();
        container.addView(inflater.inflate(R.layout.row_country_header, container, false));
        for (int i = 0; i < max; i++) {
            CountryStat.Country c = stat.countries.get(i);
            View row = inflater.inflate(R.layout.row_country, container, false);
            ((TextView) row.findViewById(R.id.country_code)).setText(String.format("%s - %s", c.key, getCountryLabel(c.key)));
            ((TextView) row.findViewById(R.id.country_requests)).setText(String.valueOf(c.requests));
            ((TextView) row.findViewById(R.id.country_threats)).setText(String.valueOf(c.threats));
            container.addView(row);
        }
        if (lastRow) {
            View row = inflater.inflate(R.layout.row_country_last, container, false);
            ((TextView) row.findViewById(R.id.country_last)).setText(String.format("%d more", stat.countries.size()-MAX));
            container.addView(row);
        }
    }

    private static String getCountryLabel(String key) {
        if (countryList == null) return "";
        try {
            return countryList.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return "?";
        }
    }

    public static void loadCountryList(Context context) {
        try {
            InputStream is = context.getResources().openRawResource(R.raw.country_list);
            byte[] b = new byte[is.available()];
            is.read(b);
            WorldMap.countryList = new JSONObject(new String(b));
        } catch (Exception e) {
            WorldMap.countryList = null;
            e.printStackTrace();
            Toast.makeText(context, R.string.error_loading_country_name, Toast.LENGTH_SHORT).show();
            Log.d("COUNTRY", "onDraw: cannot parse country list");
        }
    }

}
